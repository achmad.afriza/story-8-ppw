# Story 8
This Gitlab repository is the result of the work from **Achmad Afriza Wibawa**

## Pipeline and Coverage
[![pipeline status](https://gitlab.com/achmad.afriza/story-8-ppw/badges/master/pipeline.svg)](https://gitlab.com/achmad.afriza/story-8-ppw/commits/master)
[![coverage report](https://gitlab.com/achmad.afriza/story-8-ppw/badges/master/coverage.svg)](https://gitlab.com/achmad.afriza/story-8-ppw/commits/master)

## URL
This story can be accessed from [https://story8ppw-achmadafriza.herokuapp.com](https://story8ppw-achmadafriza.herokuapp.com)

## Author
**Achmad Afriza Wibawa** - [achmad.afriza](https://gitlab.com/achmad.afriza)
