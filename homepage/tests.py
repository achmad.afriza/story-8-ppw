from django.http import HttpRequest
from django.test import TestCase, LiveServerTestCase
from django.urls import reverse, resolve
from django.conf import settings
from importlib import import_module
from django.contrib import admin

from .apps import HomepageConfig
from .views import index

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class HomepageTests(TestCase):
    def setUp(self):
        # http://code.djangoproject.com/ticket/10899
        settings.SESSION_ENGINE = 'django.contrib.sessions.backends.file'
        engine = import_module(settings.SESSION_ENGINE)
        store = engine.SessionStore()
        store.save()
        self.session = store
        self.client.cookies[settings.SESSION_COOKIE_NAME] = store.session_key
    
    def test_apps(self):
        self.assertEqual(HomepageConfig.name, 'homepage')
    
    def test_homepage_status_code(self):
        response = self.client.get('')
        self.assertEquals(response.status_code, 200)

    def test_view_url_by_name(self):
        response = self.client.get(reverse('homepage:index'))
        self.assertEquals(response.status_code, 200)

    def test_view_uses_correct_template(self):
        response = self.client.get(reverse('homepage:index'))
        self.assertTemplateUsed(response, 'homepage.html')

    def test_homepage_contains_correct_html(self):
        response = self.client.get('')
        self.assertContains(response, "<title>Afriza's StoryEight</title>")

    def test_homepage_does_not_contain_incorrect_html(self):
        response = self.client.get('')
        self.assertNotContains(response, '{% extends "base.html" %}')
    
    def test_homepage_uses_correct_view(self):
        func = resolve('/')
        self.assertEqual(index, func.func)

class Story8_FunctionalTest(LiveServerTestCase):
    def setUp(self) :
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.driver = webdriver.Chrome(chrome_options=chrome_options, executable_path='./chromedriver')
    
    def tearDown(self) :
        self.driver.quit()
        super().tearDown()
    
    def test_two_go_up(self):
        self.driver.get(self.live_server_url)
        
        nodes = self.driver.find_elements_by_class_name("container-wrapper")
        self.driver.find_element_by_id('two').find_element_by_class_name('button-up').click()
        # self.driver.find_element_by_xpath("/html/body/div/div/div/div[2]/div[2]")
        # self.driver.find_element_by_xpath("//div[@class='status-wrapper']/div[2]")
        after = self.driver.find_elements_by_class_name("container-wrapper")

        self.assertEqual(nodes[1].id, after[0].id)
    def test_two_go_down(self):
        self.driver.get(self.live_server_url)
        
        nodes = self.driver.find_elements_by_class_name("container-wrapper")
        self.driver.find_element_by_id('two').find_element_by_class_name('button-down').click()
        # self.driver.find_element_by_xpath("/html/body/div/div/div/div[2]/div[2]")
        # self.driver.find_element_by_xpath("//div[@class='status-wrapper']/div[2]")
        after = self.driver.find_elements_by_class_name("container-wrapper")

        self.assertEqual(nodes[1].id, after[2].id)
    def test_one_go_up(self):
        self.driver.get(self.live_server_url)
        
        nodes = self.driver.find_elements_by_class_name("container-wrapper")
        self.driver.find_element_by_id('one').find_element_by_class_name('button-up').click()
        # self.driver.find_element_by_xpath("/html/body/div/div/div/div[2]/div[2]")
        # self.driver.find_element_by_xpath("//div[@class='status-wrapper']/div[1]")
        after = self.driver.find_elements_by_class_name("container-wrapper")

        self.assertEqual(nodes[0].id, after[0].id)
    # def test_five_go_down(self):
    #     self.driver.get(self.live_server_url)
    #     self.waitUntilLoaded()
        
    #     nodes = self.driver.find_elements_by_class_name("container-wrapper")
    #     # self.driver.find_element_by_id('five').find_element_by_class_name('button-down').click()
    #     self.driver.find_element_by_xpath("/html/body/div/div/div/div[2]/div[5]/div[1]/div[2]/svg[2]").click()
    #     # self.driver.find_element_by_xpath("//div[@class='status-wrapper']/div[5]")
    #     after = self.driver.find_elements_by_class_name("container-wrapper")

    #     self.assertEqual(nodes[4].id, after[4].id)
    def test_accordion(self):
        self.driver.get(self.live_server_url)
        
        element = self.driver.find_element_by_xpath("//div[@class='status-wrapper']/div[1]")
        element.find_element_by_class_name('button-plus').click()
        css_value = element.find_element_by_class_name('status-content').value_of_css_property('display')

        self.assertEqual(css_value, 'block')
    
    def waitUntilLoaded(self):
        self.driver.implicitly_wait(30)