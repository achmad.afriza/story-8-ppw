from django.shortcuts import render, redirect
from django.contrib import messages
from django.core import validators
from django.urls import reverse

# Create your views here.
def index(request):
    context = {
        'nav': [
            [reverse('homepage:index'), 'Home'],
            ['https://story5ppw-achmadafriza.herokuapp.com/', 'About Me'],
        ]
    }
    
    return render(request, 'homepage.html', context)