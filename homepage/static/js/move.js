$(".button-up").on('click', function(){
    var nodes = $(".container-wrapper")
    var childContainer = $(this).closest('div.container-wrapper')
    var index = $(nodes).index(childContainer)

    if(index != 0){
        $(childContainer).insertBefore($(nodes).get(index-1))
    }
})

$(".button-down").on('click', function(){
    var nodes = $(".container-wrapper")
    var childContainer = $(this).closest('div.container-wrapper')
    var index = $(nodes).index(childContainer)

    if(index != nodes.length-1){
        $(childContainer).insertAfter($(nodes).get(index+1))
    }
})