$(".button-plus").on('click', function(){
    var parentContainer = $(this).closest('div.container-wrapper')
    if($(parentContainer).find(".status-content").css("display") === "none") {
        $(parentContainer).find(".status-content").css("display", "block")
        $(parentContainer).find(".button-minus").css("display", "initial")
        $(parentContainer).find(".button-plus").css("display", "none")
    }
})

$(".button-minus").on('click', function(){
    var parentContainer = $(this).closest('div.container-wrapper')
    if($(parentContainer).find(".status-content").css("display") === "block") {
        $(parentContainer).find(".status-content").css("display", "none")
        $(parentContainer).find(".button-minus").css("display", "none")
        $(parentContainer).find(".button-plus").css("display", "initial")
    }
})